import path from 'path'
import { BannerPlugin, Configuration, DefinePlugin } from 'webpack'
import nodeExternals from 'webpack-node-externals'

const config: Configuration = {
  mode: 'production',
  target: 'node',
  externals: [nodeExternals()],
  entry: './index.ts',
  devtool: 'inline-source-map',
  context: path.resolve(__dirname, 'src'),
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: ['ts-loader'],
        include: [path.resolve(__dirname, 'src')],
        exclude: [/node_modules/, '../webpack.config.ts'],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  output: {
    filename: 'pad.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new BannerPlugin({
      banner: '#!/usr/bin/env node',
      raw: true,
      entryOnly: true,
    }),
    new DefinePlugin({
      'process.env': {
        VERSION: JSON.stringify(process.env.npm_package_version),
        DESCRIPTION: JSON.stringify(process.env.npm_package_description),
      },
    }),
  ],
  watchOptions: {
    poll: true,
  },
}

export default config
