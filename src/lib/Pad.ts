import { Chalk } from 'chalk'
import { ChildProcess, spawn } from 'child_process'

import Action from './Action'
import LaunchOptions from './LaunchOptions'
import PadOptions from './PadOptions'
import { createPipeline } from './pipeline'

export type WritableStream = NodeJS.WritableStream

export default class Pad {
  constructor(private padOptions: PadOptions) {}

  public launch(launchOptions: LaunchOptions) {
    for (const action of launchOptions.sequence) {
      this.spawn(action)
    }
  }

  private spawn(action: Action) {
    const { command, args = [], cwd = '.' } = action
    const { stdio, out, err } = this.prepareIO(action)

    this.notify(out, `SPAWNING - '${[command, ...args].join(' ')}' at ${cwd}`)
    const child = spawn(command, args, { stdio, cwd })
    this.connectToChildProcess(child, out, err)

    return child
  }

  private prepareIO(action: Action) {
    const { name } = action

    const {
      padio: { padout = process.stdout, paderr = process.stderr },
      styles: { info, error },
      template,
    } = this.padOptions

    const out = createPipeline(padout, template, { name, type: 'OUT' }, info)
    const err = createPipeline(paderr, template, { name, type: 'ERR' }, error)

    return { stdio: [undefined, 'pipe', 'pipe'], out, err }
  }

  private notify(
    stream: WritableStream,
    message: string,
    style: Chalk = this.padOptions.styles.notice
  ) {
    stream.write(style(message))
  }

  private connectToChildProcess(
    child: ChildProcess,
    out: WritableStream,
    err: WritableStream
  ) {
    const pipeOptions = { end: false }
    child.stdout.pipe(
      out,
      pipeOptions
    )
    child.stderr.pipe(
      err,
      pipeOptions
    )

    const notifyWithSignal = (type: string, code: number, signal: string) =>
      this.notify(out, `${type} - code: ${code}, signal: ${signal}`)

    child.on('close', (code, signal) => notifyWithSignal('CLOSE', code, signal))
    child.on('exit', (code, signal) => notifyWithSignal('EXIT', code, signal))
    child.on('disconnect', () => this.notify(out, 'DISCONNECT'))
    child.on('error', (error) =>
      this.notify(
        err,
        error.stack || error.message,
        this.padOptions.styles.error
      )
    )
  }
}
