import { Chalk } from 'chalk'

import format from './format'
import { WritableStream } from './Pad'
import { Replacements } from './Replacements'
import Splicer from './splicer'

export function createPipeline(
  target: WritableStream,
  template: string,
  replacements: Replacements,
  style: Chalk
): WritableStream {
  const formatted = style(format(template, replacements))
  const source = new Splicer({ prefix: formatted })
  source.pipe(target)
  return source
}
