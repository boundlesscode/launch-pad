import { Chalk } from 'chalk'

export default interface PadOptions {
  padio: {
    padin: NodeJS.ReadableStream
    padout: NodeJS.WritableStream
    paderr: NodeJS.WritableStream
  }
  styles: {
    info: Chalk
    error: Chalk
    notice: Chalk
  }
  template: string
}
