import Action from './Action'

export default interface LaunchOptions {
  mode?: 'parallel'
  sequence: Action[]
}
