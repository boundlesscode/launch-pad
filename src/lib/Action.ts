export default interface Action {
  name: string
  command: string
  args?: ReadonlyArray<string>
  cwd?: string
}
