import { Replacements } from './Replacements'

const PATTERN = /\{\{[^\}]+\}\}/g

export default function format(template: string, replacements: Replacements) {
  return template.replace(PATTERN, (match) => {
    const key = match.slice(2, -2).trim()
    return replacements.hasOwnProperty(key) ? replacements[key] : match
  })
}
