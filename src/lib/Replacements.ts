export interface Replacements {
  [key: string]: string
}
