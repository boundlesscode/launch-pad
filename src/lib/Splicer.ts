import { Transform, TransformCallback, TransformOptions } from 'stream'

const SPLITTER = /[\n\r]+/g
const SEPARATOR = '\n'

export interface PrependOptions extends TransformOptions {
  prefix: string | ''
}

export default class Splicer extends Transform {
  private _lastPart: string | null
  private _prefix: string

  constructor(prependOptions: PrependOptions) {
    const options = {
      ...prependOptions,
      objectMode: true,
    }
    super(options)

    this._prefix = prependOptions.prefix

    this._lastPart = null
  }

  public _transform(
    chunk: any,
    encoding: string,
    callback: TransformCallback
  ): void {
    // convert chunk to array of strings
    const chunkParts = chunk.toString().split(SPLITTER)

    const [head, ...tail] = chunkParts

    const parts = [
      // prepend previous last part
      (this._lastPart || '') + head,

      // append all but the new last part
      ...tail.slice(0, -1),
    ]

    this._lastPart = tail[tail.length - 1]

    this.pushParts(parts)

    callback()
  }

  public _flush(callback: TransformCallback) {
    if (this._lastPart) {
      this.pushParts([this._lastPart])

      this._lastPart = null
    }

    callback()
  }

  private pushParts(parts: string[]) {
    const render = (part: string) => `${this._prefix}${part}${SEPARATOR}`
    parts.map(render).forEach((part) => this.push(part))
  }
}
