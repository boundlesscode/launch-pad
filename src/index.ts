import program from 'commander'

import './polyfills'

program
  .version(process.env.VERSION || 'n/a', '-v, --version')
  .description(process.env.DESCRIPTION || '')

program.parse(process.argv)

if (!process.argv.slice(2).length) {
  program.outputHelp()
}
